
" Jorden Wilder's theoretical option price based on the Black - Scholes model."
" This model determines the price of a US style call or put option"
" The user must input the strike price (X), the underlying price (S), the time untill expirery in years (T), the risk free interest rate (r) and volatility (V)"
" This model determines 'The Greeks' of an option - Delta, Gamma, Vega, Theta, Rho"
" This model calculates different spread bets for options - backspreads, strangles (+ straddles), butterflys, time spreads and ladders "

#imports
import numpy as np, math
from scipy.stats import norm


#Option base class
class option:
    
    #member data
    X = 0
    S = 0
    V = 0
    T = 0
    r = 0
    delta = 0
    gamma = 0
    vega = 0
    theta = 0
    rho = 0
    d_1 = 0
    d_2 = 0
    cp = ""
    
    #default constructor
    def __init__(self):
        print('Option default constructor called')
        
    #parameterized constructor
    def __init__(self, strike, underlying, volatility, time, interest):
        self.X = strike
        self.S = underlying
        self.V = volatility
        self.T = time
        self.r = interest
        self.d_1 = (np.log(self.S/self.X)+((self.V*self.V)/2)*self.T)/(self.V*math.sqrt(self.T))
        self.d_2 = self.d_1 - self.V*math.sqrt(self.T)
        self.gamma = norm.cdf(self.d_1)/(self.S*self.V*math.sqrt(self.T))
        self.vega = self.S*math.sqrt(self.T)*norm.cdf(self.d_1)

        print('Option parameterized constructor called')
        
    #destructor
    def __del__(self):
        print('Option destructor called')
    
    #function to return the strike of an option
    def o_strike(self):
        print (self.X)
        return self.X
    
    #function to return the underlying of an option
    def o_underlying(self):
        print (self.S)
        return self.S
    
    #function to return the volatility of an option
    def o_volatility(self):
        print (self.V)
        return self.V
    
    #function to return the time to maturity of an option
    def o_time(self):
        print (self.t)
        return (self.t)
    
    #function to return the risk free interest rate of an option
    def o_risk(self):
        print (self.r)
        return (self.r)
    
    #function to return the delta of an option
    def o_delta(self):
        print(self.delta)
        return self.delta
    
    #function to return the gamma of an option
    def o_gamma(self):
        print(self.gamma)
        return self.gamma
        
    #function to return the vega of an option
    def o_vega(self):
        print(self.vega)
        return self.vega
        
    #function to return the theta of an option
    def o_theta(self):
        print(self.theta)
        return self.theta
        
    #function to return the rho of an option
    def o_rho(self):
        print(self.rho)
        return self.rho
        
    #function to return the d_1 of an option
    def o_d_1(self):
        print(self.d_1)
        return self.d_1
    
    #function to return the d_2 of an option
    def o_d_2(self):
        print(self.d_2)
        return self.d_2    
    
    #function to return the type of option
    def o_type(self):
        print(self.cp)
        return self.cp


#Derived class call option
class call(option):
    
    #member type is call
    cp = "call"
    
    
    
    #override constructor
    def __init__(self):
        option.__init___()
        self.delta = norm.cdf(self.d_1)
        self.theta = -(self.S*self.V*norm(self.d_1))/(2*math.sqrt(self.T)) - self.r*self.X*math.exp(-self.r*self.T)*norm(self.d_2)
        self.rho = self.X*self.T*math.exp(-self.r*self.T)*norm(self.d_2)
        print('Call default constructor called')
    
    #override parameterized constructor
    def __init__(self, strike, underlying, volatility, time, interest):
        option.__init__(self,strike,underlying,volatility,time,interest)
        self.theta = -(self.S*self.V*norm.cdf(self.d_1))/(2*math.sqrt(self.T)) - self.r*self.X*math.exp(-self.r*self.T)*norm.cdf(self.d_2)
        self.rho = self.X*self.T*math.exp(-self.r*self.T)*norm.cdf(self.d_2)
        self.delta = norm.cdf(self.d_1)
        print('Call parameterized constructor called')
    
    #override destructor
    def __del__(self):
        print ('Call destructor called')
    
    #function to calculate the theoretical price of a call option
    def price(self):
        price =  math.exp(-self.r*self.T)*(self.S*norm.cdf(self.d_1)-self.X*norm.cdf(self.d_2))
        print (price)
        return price

    

#Derived class put option
class put(option):
    
    #member type is put
    cp = "put"
    
    #override constructor
    def __init__(self):
        option.__init__()
        self.delta = norm.cdf(self.d_1) - 1
        self.theta = self.r*self.X*math.exp(-self.r*self.T)*norm.cdf(-self.d_2) -(self.S*self.V*norm.cdf(self.d_1))/(2*math.sqrt(self.T)) 
        self.rho = -self.X*self.T*math.exp(-self.r*self.T)*norm.cdf(-self.d_2)
        
        print('Put parameterized constructor called')
        
    #override parameterized contsructor
    def __init__(self, strike, underlying, volatility, time, interest):
        option.__init__(self,strike,underlying,volatility,time,interest)
        self.delta = norm.cdf(self.d_1) - 1
        self.theta = self.r*self.X*math.exp(-self.r*self.T)*norm.cdf(-self.d_2) -(self.S*self.V*norm.cdf(self.d_1))/(2*math.sqrt(self.T)) 
        self.rho = -self.X*self.T*math.exp(-self.r*self.T)*norm.cdf(-self.d_2)
        print('Put parameterized constructor called')
    
    #override destructor
    def __del__(self):
        print('Put destructor called')
    
    #function to calculate the theoretical price of a put option
    def price(self):
        price = math.exp(-self.r*self.T)*(self.X*norm.cdf(-self.d_2)-self.S*norm.cdf(-self.d_1))
        print (price)
        return price



#Base class spread
class spread:
    
    #member data
    o1 = 0
    o2 = 0
    delta = 0
    gamma = 0
    vega = 0
    theta = 0
    rho = 0
    PL = 0
    ls = ""
    #default constructor
    def __init__(self):
        print('Spread default constructor called')
    
    #destructor 
    def __del__(self):
        print('Spread destructor called')
    
    #function to return the delta of a spread
    def s_delta(self):
        print(self.delta)
        return self.delta
    
    #function to return the gamma of a spread
    def s_gamma(self):
        print(self.gamma)
        return self.gamma
        
    #function to return the vega of a spread
    def s_vega(self):
        print(self.vega)
        return self.vega
        
    #function to return the theta of a spread
    def s_theta(self):
        print(self.theta)
        return self.theta
        
    #function to return the rho of a spread
    def s_rho(self):
        print(self.rho)
        return self.rho
    
    #function to return the theoretical edge (PL) of a spread
    def s_PL(self):
        print(self.PL)
        return self.PL 

    #function to retrun the delta risk of a spread
    def delta_risk(self):
        print(self.delta/self.PL)
        return self.delta/self.PL
    
    #function to retrun the gamma risk of a spread
    def gamma_risk(self):
        print(self.gamma/self.PL)
        return self.gamma/self.PL
    
    #function to retrun the vega risk of a spread
    def vega_risk(self):
        print(self.vega/self.PL)
        return self.vega/self.PL
    
    #function to retrun the theta risk of a spread
    def theta_risk(self):
        print(self.theta/self.PL)
        return self.theta/self.PL
    
    #function to retrun the theta risk of a spread
    def rho_risk(self):
        print(self.rho/self.PL)
        return self.rho/self.PL



#Volatility spreads




#Derived class backspread (long and short)
class backspread(spread):
    
    #default constructor
    def __init__(self):
        print('Backspread default constructor called')
        
    #parameterized constructor
    def __init__(self, Nbuy, buy_option, Nsell, sell_option):
        self.o1 = buy_option
        self.o2 = sell_option
        self.delta = Nbuy*self.o1.delta - Nsell*self.o2.delta
        self.gamma = Nbuy*self.o1.gamma - Nsell*self.o2.gamma
        self.vega = Nbuy*self.o1.vega - Nsell*self.o2.vega
        self.theta = Nbuy*self.o1.theta - Nsell*self.o2.theta
        self.rho = Nbuy*self.o1.rho - Nsell*self.o2.rho
        self.PL = Nsell*sell_option.price() - Nbuy*buy_option.price()
        print('Backspread parameterized constructor called')
    
    #destructor 
    def __del__(self):
        print('Backspread destructor called')
        
        
#Derived class strangle (incldues straddle)
class strangle(spread):
    
    #default constructor
    def __init__(self):
        print('Strangle default constructor called')
    
    #parameterized constructor
    def __init__(self, Ncall, call_option, Nput, put_option, ls):
        self.ls = ls
        if (ls == "long"):
            self.o1 = call_option
            self.o2 = put_option
            self.delta = Ncall*self.o1.delta + Nput*self.o2.delta
            self.gamma = Ncall*self.o1.gamma + Nput*self.o2.gamma
            self.vega = Ncall*self.o1.vega + Nput*self.o2.vega
            self.theta = Ncall*self.o1.theta + Nput*self.o2.theta
            self.rho = Ncall*self.o1.rho + Nput*self.o2.rho
            self.PL = -Ncall*call_option.price() - Nput*put_option.price()
            print('Long strangle parameterized constructor called')
        elif (ls == "short"):
            self.o1 = call_option
            self.o2 = put_option
            self.delta = -Ncall*self.o1.delta - Nput*self.o2.delta
            self.gamma = -Ncall*self.o1.gamma - Nput*self.o2.gamma
            self.vega = -Ncall*self.o1.vega - Nput*self.o2.vega
            self.theta = -Ncall*self.o1.theta - Nput*self.o2.theta
            self.rho = -Ncall*self.o1.rho - Nput*self.o2.rho
            self.PL = Ncall*call_option.price() + Nput*put_option.price()
            print('Short strangle parameterized constructor called')
        

    #destructor 
    def __del__(self):
        if (self.ls == "long"):
            print('Long strangle destructor called')
        elif (self.ls == "short"):
            print('Short strangle destructor called')



#Derived class butterfly
class fly(spread):
    
    #Member data for third option
    o3 = 0
    
    #default constructor
    def __init__(self):
        print('Butterfly default constructor called')
    
    #parameterized constructor
    def __init__(self, Nbig, low_option, middle_option, high_option, ls):
        self.ls = ls
        if (ls == "long"):
            self.o1 = low_option
            self.o2 = middle_option
            self.o3 = high_option
            self.delta = (Nbig/2)*self.o1.delta - Nbig*self.o2.delta + (Nbig/2)*self.o3.delta 
            self.gamma = (Nbig/2)*self.o1.gamma - Nbig*self.o2.gamma + (Nbig/2)*self.o3.gamma
            self.vega = (Nbig/2)*self.o1.vega - Nbig*self.o2.vega + (Nbig/2)*self.o3.vega
            self.theta = (Nbig/2)*self.o1.theta - Nbig*self.o2.theta + (Nbig/2)*self.o3.theta
            self.rho = (Nbig/2)*self.o1.rho - Nbig*self.o2.rho + (Nbig/2)*self.o3.rho
            self.PL = -(Nbig/2)*self.o1.price() + Nbig*self.o2.price() - (Nbig/2)*self.o3.price()
            print('Long butterfly parameterized constructor called')
        elif (ls == "short"):
            self.o1 = low_option
            self.o2 = middle_option
            self.o3 = high_option
            self.delta = -(Nbig/2)*self.o1.delta + Nbig*self.o2.delta - (Nbig/2)*self.o3.delta 
            self.gamma = -(Nbig/2)*self.o1.gamma + Nbig*self.o2.gamma - (Nbig/2)*self.o3.gamma
            self.vega = -(Nbig/2)*self.o1.vega + Nbig*self.o2.vega - (Nbig/2)*self.o3.vega
            self.theta = -(Nbig/2)*self.o1.theta + Nbig*self.o2.theta - (Nbig/2)*self.o3.theta
            self.rho = -(Nbig/2)*self.o1.rho + Nbig*self.o2.rho - (Nbig/2)*self.o3.rho
            self.PL = (Nbig/2)*self.o1.price() - Nbig*self.o2.price() + (Nbig/2)*self.o3.price()
            print('Short butterfly parameterized constructor called')
    
        #destructor 
        def __del__(self):
            if (self.ls == "long"):
                print('Long butterfly destructor called')
            elif (self.ls == "short"):
                print('Short butterfly destructor called')
                
                
                
#Derived class time spread (includes diagonal spread)
class time_spread(spread):
    
    #default constructor
    def __init__():
        print('Time spread default constructor called')
    
    
    #parameterized constructor
        def __init__(self, Nbuy, buy_option, Nsell, sell_option):
            self.o1 = buy_option
            self.o2 = sell_option
            self.delta = Nbuy*self.o1.delta - Nsell*self.o2.delta
            self.gamma = Nbuy*self.o1.gamma - Nsell*self.o2.gamma
            self.vega = Nbuy*self.o1.vega - Nsell*self.o2.vega
            self.theta = Nbuy*self.o1.theta - Nsell*self.o2.theta
            self.rho = Nbuy*self.o1.rho - Nsell*self.o2.rho
            self.PL = Nsell*sell_option.price() - Nbuy*buy_option.price()
            print('Time spread parameterized constructor called')

    #destructor 
    def __del__(self):
        print('Time spread destructor called')


#Derived class ladder
class ladder(spread):
    
    #Member data for third option
    o3 = 0
    
    #default constructor
    def __init__(self):
        print('Ladder default constructor called')
    
    #parameterized constructor
    def __init__(self,  low_option, middle_option, high_option,ls):
        
        self.ls = ls
        
        if (ls == "long"):
            if (low_option.o_type() == "call"):
                self.o1 = low_option
                self.o2 = middle_option
                self.o3 = high_option
                self.delta = self.o1.delta - self.o2.delta - self.o3.delta 
                self.gamma = self.o1.gamma - self.o2.gamma - self.o3.gamma
                self.vega = self.o1.vega - self.o2.vega - self.o3.vega
                self.theta = self.o1.theta- self.o2.theta - self.o3.theta
                self.rho = self.o1.theta - self.o2.theta - self.o3.theta
                self.PL = -self.o1.price() + self.o2.price() + self.o3.price()
            elif (low_option.o_type() == "put"):
                self.o1 = low_option
                self.o2 = middle_option
                self.o3 = high_option
                self.delta = -self.o1.delta - self.o2.delta + self.o3.delta 
                self.gamma = -self.o1.gamma - self.o2.gamma + self.o3.gamma
                self.vega = -self.o1.vega - self.o2.vega + self.o3.vega
                self.theta = -self.o1.theta- self.o2.theta + self.o3.theta
                self.rho = -self.o1.theta - self.o2.theta + self.o3.theta
                self.PL = self.o1.price() + self.o2.price() - self.o3.price()
            print('Long ladder parameterized constructor called')
        
        elif (ls == "short"):
            if (low_option.o_type() == "call"):
                self.o1 = low_option
                self.o2 = middle_option
                self.o3 = high_option
                self.delta = -self.o1.delta + self.o2.delta + self.o3.delta 
                self.gamma = -self.o1.gamma + self.o2.gamma + self.o3.gamma
                self.vega = -self.o1.vega + self.o2.vega + self.o3.vega
                self.theta = -self.o1.theta + self.o2.theta + self.o3.theta
                self.rho = -self.o1.theta + self.o2.theta + self.o3.theta
                self.PL = self.o1.price() - self.o2.price() - self.o3.price()
            elif (low_option.o_type() == "put"):
                self.o1 = low_option
                self.o2 = middle_option
                self.o3 = high_option
                self.delta = self.o1.delta + self.o2.delta - self.o3.delta 
                self.gamma = self.o1.gamma + self.o2.gamma - self.o3.gamma
                self.vega = self.o1.vega + self.o2.vega - self.o3.vega
                self.theta = self.o1.theta + self.o2.theta - self.o3.theta
                self.rho = self.o1.theta + self.o2.theta - self.o3.theta
                self.PL = -self.o1.price() - self.o2.price() + self.o3.price()
            print('Short ladder parameterized constructor called')
    
        #destructor 
        def __del__(self):
            if (self.ls == "long"):
                print('Long ladder destructor called')
            elif (self.ls == "short"):
                print('Short ladder destructor called')

#Bull/Bear spread

#
class bull_bear(spread):
    
    bb = ""
    #default constructor
    def __init__(self):
        print('Bull/Bear default constructor called')
        
    #parameterized constructor
    def __init__(self, Ncall, call_option, Nput, put_option, bb):
        self.bb = bb
        self.o1 = call_option
        self.o2 = put_option
        if (bb == 'bull'):
            self.delta = Ncall*self.o1.delta - Nput*self.o2.delta
            self.gamma = Ncall*self.o1.gamma - Nput*self.o2.gamma
            self.vega = Ncall*self.o1.vega - Nput*self.o2.vega
            self.theta = Ncall*self.o1.theta - Nput*self.o2.theta
            self.rho = Ncall*self.o1.rho - Nput*self.o2.rho
            self.PL = Nput*put_option.price() - Ncall*call_option.price()
            print('Bull spread parameterized constructor called')
        elif (bb == 'bear'):
            self.delta = -Ncall*self.o1.delta + Nput*self.o2.delta
            self.gamma = -Ncall*self.o1.gamma + Nput*self.o2.gamma
            self.vega = -Ncall*self.o1.vega + Nput*self.o2.vega
            self.theta = -Ncall*self.o1.theta + Nput*self.o2.theta
            self.rho = -Ncall*self.o1.rho + Nput*self.o2.rho
            self.PL = -Nput*put_option.price() + Ncall*call_option.price()
            print('Bear spread parameterized constructor called')
    
    #destructor 
    def __del__(self):
        if (self.bb == "bull"):
            print('Bull spread destructor called')
        elif (self.bb == "bear"):
            print('Bear spread  destructor called')













    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
